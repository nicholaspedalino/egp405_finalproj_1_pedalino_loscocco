#include "Asteroid.h"

Asteroid::Asteroid(Vector2 pos, AsteroidSize size, Vector2 velocity, sf::Color outlineColor, sf::RenderWindow* window)
{
	mPosition = pos;
	mSize = size;
	mVelocity = velocity;
	mOutlineColor = outlineColor;
	mWindow = window;

	mCircleShape = sf::CircleShape((float)size * SIZE_MOD);
	mCircleShape.setOutlineThickness(1.0f);
	mCircleShape.setOutlineColor(mOutlineColor);
	mCircleShape.setFillColor(FILL_COLOR);
	mCircleShape.setPosition(mPosition);
	mCircleShape.setOrigin((float)size * SIZE_MOD, (float)size * SIZE_MOD);
	markedForDeath = false;
}

Asteroid::~Asteroid()
{
}

void Asteroid::Update(float deltaTime)
{
	mPosition += mVelocity;
	mCircleShape.setPosition(mPosition);
}

void Asteroid::UpdateInterpolation(float deltaTime)
{

}

void Asteroid::Draw()
{
	mWindow->draw(mCircleShape);
}

void Asteroid::OnSyncSend(RakNet::BitStream& bsOut)
{

}

void Asteroid::OnSyncReceive(RakNet::BitStream& bsIn)
{

}

bool Asteroid::CheckCollisionWithBullet(Bullet* b)
{
	//distance checks :(
	//both the bullet and this asteroid are circles with centered origins
	float dx = b->getPosition().x - mPosition.x;
	float dy = b->getPosition().y - mPosition.y;
	float radii = b->GetRadius() + (mSize * SIZE_MOD);//our radius is just the size * the size mod
	
		   //a^2 + b^2 < c^2 when a bullet is inside an asteroid
	return ((dx * dx) + (dy * dy) < (radii * radii));
}