#ifndef ASTEROID_H
#define ASTEROID_H

#include "WowObject.h"
#include "Bullet.h"
#include "SFML\Graphics.hpp"

enum AsteroidSize
{
	SMALL_ASTEROID = 1,
	MEDIUM_ASTEROID = 2,
	LARGE_ASTEROID = 3,
	GIANT_ASTEROID = 4
};

class Asteroid : public WowObject
{
public:
	Asteroid(Vector2 pos, AsteroidSize size, Vector2 velocity, sf::Color outlineColor, sf::RenderWindow* window);
	~Asteroid();

	virtual void Update(float deltaTime);
	virtual void UpdateInterpolation(float deltaTime);
	virtual void Draw();

	virtual void OnSyncSend(RakNet::BitStream& bsOut) override;
	virtual void OnSyncReceive(RakNet::BitStream& bsIn) override;

	bool CheckCollisionWithBullet(Bullet* b);

	bool markedForDeath;

	void SetAsteroidID(int id){ mAsteroidID = id; }
	int GetAsteroidID(){ return mAsteroidID; }

	AsteroidSize GetAsteroidSize(){ return mSize; }
	Vector2 GetVelocity(){ return mVelocity; }

	float GetRadius(){ return mCircleShape.getRadius(); }

private:
	sf::CircleShape mCircleShape;
	sf::Color mOutlineColor;
	const sf::Color FILL_COLOR = sf::Color::Transparent;
	AsteroidSize mSize;
	Vector2 mVelocity;
	sf::RenderWindow* mWindow;

	const float SIZE_MOD = 10.0f; //multiply mAsteroidSize enum by this to get the actual radius

	int mAsteroidID;
};


#endif