#include "Bullet.h"

# define M_PI           3.14159265358979323846  /* pi */

Bullet::Bullet(Vector2 pos, float rotation, float speed, sf::Color outlineColor, sf::RenderWindow* window)
{
	mObjectID = ID_BULLET;
	mPosition = pos;
	mRotation = rotation;
	mSpeed = speed;
	mOutlineColor = outlineColor;
	mWindow = window;

	//set up the rect shape
	mCircleShape = sf::CircleShape((float)mRadius);
	mCircleShape.setFillColor(mFillColor);
	mCircleShape.setOutlineColor(mOutlineColor);
	mCircleShape.setOutlineThickness((float)mOutlineThickness);
	mCircleShape.setOrigin((float)mRadius, (float)mRadius);//origin at bottom center
	//mCircleShape.setRotation(mRotation); --its a circle now
	mCircleShape.setPosition(mPosition);

	//set vel based on speed and rotation
	mVelocity.x = cos(mRotation * (float)M_PI / 180.0f) * mSpeed;
	mVelocity.y = sin(mRotation * (float)M_PI / 180.0f) * mSpeed;

	ageS = 0.0f;
	markedForDeath = false;
}

Bullet::~Bullet()
{
}

void Bullet::Update(float deltaTime)
{
	ageS += deltaTime;

	mPosition += mVelocity;

	mCircleShape.setPosition(mPosition);
}

void Bullet::Draw()
{
	mWindow->draw(mCircleShape);
}

void Bullet::OnSyncSend(RakNet::BitStream& bsOut)
{

}

void Bullet::OnSyncReceive(RakNet::BitStream& bsIn)
{

}