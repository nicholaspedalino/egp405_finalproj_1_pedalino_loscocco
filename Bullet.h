#ifndef BULLET_H
#define BULLET_H

#include "WowObject.h"

class Bullet : public WowObject
{
public:
	Bullet(Vector2 pos, float rotation, float speed, sf::Color outlineColor, sf::RenderWindow* window);
	~Bullet();

	virtual void Update(float deltaTime);
	virtual void Draw();

	virtual void OnSyncSend(RakNet::BitStream& bsOut) override;
	virtual void OnSyncReceive(RakNet::BitStream& bsIn) override;

	inline float GetAge(){ return ageS; }
	inline float GetRotation(){ return mRotation; }
	inline float GetRadius(){ return (float)mRadius; }

	inline void SetBulletID(int newID){ mBulletID = newID; }
	inline int GetBulletID(){ return mBulletID; }

	bool markedForDeath;

	Vector2 GetVelocity(){ return mVelocity; }

private:
	float mRotation;//degrees
	float mSpeed;

	//sprite variables
	sf::CircleShape mCircleShape;
	const int mRadius = 1;
	sf::Color mOutlineColor;
	const int mOutlineThickness = 1;
	const sf::Color mFillColor = sf::Color::Black;

	//target render window
	sf::RenderWindow* mWindow;

	float ageS;//how old am I? in seconds

	int mBulletID;

	Vector2 mVelocity;
};

#endif