#include "GameManager.h"
#include "BitStream.h"

#include "GetTime.h"

# define M_PI           3.14159265358979323846  /* pi */

GameManager* GM;

GameManager::GameManager()
{
}

GameManager::~GameManager()
{
	Cleanup();
}

void GameManager::Cleanup()
{
	delete mPlayer1Ship;
	delete mPlayer2Ship;
}

void GameManager::Initialize(RakNet::RakNetGUID newGUID, bool isServer)
{
	redraw = true;
	mWindow = new sf::RenderWindow(sf::VideoMode(800, 500), "BreakPong - Mike Loscocco + Nick Pedalino = <3");
	mGUID = newGUID;
	mIsServer = isServer;

	//init player 1 ship
	mPlayer1Ship = new Ship(Vector2(150.0f, 250.0f), sf::Color::Red, sf::Color::Transparent, 4.0f, mWindow);

	//init player 2 ship
	mPlayer2Ship = new Ship(Vector2(mWindow->getSize().x - 150.0f, 250.0f), sf::Color::Green, sf::Color::Transparent, 4.0f, mWindow);

	//init asteroid spawning vars
	mAsteroidSpawnTimer = 0.0f;
	mSpawnTime = 2.0f;
	mRoundNum = 1;

	minAsteroidSpeed = 0.5f;
	maxAsteroidSpeed = 1.5f;

	srand(static_cast<unsigned int>(time(NULL)));

	mAsteroids = new std::vector<Asteroid*>();

	mPlayer1NaughtyList = mPlayer1Ship->GetNaughtyList();
	mPlayer2NaughtyList = mPlayer2Ship->GetNaughtyList();

	mBulletIDCounter = 0;

	mRoundNum = 1;

	//score
	mScore = 0;
	if (!mFont.loadFromFile("../QuartzMS.ttf"))
	{//../ will look move up in the folder heirarchy.
		//printf("\nFailed to load QuartzMS.ttf from file!\n");
		if (!mFont.loadFromFile("../../QuartzMS.ttf"))
		{
			if (!mFont.loadFromFile("../../../QuartzMS.ttf"))
			{
				printf("\nFailed to load QuartzMS.ttf from file!\n");
			}
		}
	}
	mScoreText.setFont(mFont);
	mScoreText.setString("round: " + std::to_string(mRoundNum) + "\nscore: " + std::to_string(mScore));
	mScoreText.setColor(mScoreColor);
	//center score text
	sf::FloatRect scoreTextRect = mScoreText.getLocalBounds();
	mScoreText.setOrigin(scoreTextRect.left + scoreTextRect.width / 2.0f, scoreTextRect.top + scoreTextRect.height / 2.0f);
	mScoreText.setPosition(Vector2((float)mWindow->getSize().x / 2.0f, (float)mScoreSize));

	//lives
	mNumLives = TOTAL_NUM_LIVES;
	mLivesText.setFont(mFont);
	mLivesText.setString("Lives: " + std::to_string(mNumLives));
	mLivesText.setColor(mScoreColor);
	//center lives text
	sf::FloatRect livesTextRect = mLivesText.getLocalBounds();
	mLivesText.setOrigin(livesTextRect.left + livesTextRect.width / 2.0f, livesTextRect.top + livesTextRect.height / 2.0f);
	mLivesText.setPosition(Vector2((float)mWindow->getSize().x / 2.0f, (float)mWindow->getSize().y - (float)mScoreSize));

	//game over text
	isGameOver = false;
	endText.setFont(mFont);
	endText.setString("Game Over\n\n Your Score: " + std::to_string(mScore));
	endText.setColor(mScoreColor);
	//center lives text
	sf::FloatRect endTextRect = endText.getLocalBounds();
	endText.setOrigin(endTextRect.left + endTextRect.width / 2.0f, endTextRect.top + endTextRect.height / 2.0f);
	endText.setPosition(Vector2((float)mWindow->getSize().x / 2.0f, (float)mScoreSize));

	hasAKeyBeenReleased = true;

	startLerpingTimer = mDiscardTime * 2;
}

#include <iostream>
void GameManager::Update()
{
	//make this update fixed to FPS
	if (mClock.getElapsedTime().asSeconds() >= FPS)
	{
		redraw = true;
		mDeltaTime = mClock.restart();
	}
	else
	{
		sf::Time sleepTime = sf::seconds(FPS - mClock.getElapsedTime().asSeconds());
		sf::sleep(sleepTime);
		mDeltaTime += sleepTime;
	}

	//handle input
	while (mWindow->pollEvent(ev))
	{
		if (ev.type == sf::Event::Closed)
		{
			mWindow->clear();
		}
	}

	//draw and update if ready
	if (redraw)
	{
		if (mPlayer1Ship->GetOwnerGUID() != RakNet::UNASSIGNED_RAKNET_GUID && mPlayer2Ship->GetOwnerGUID() != RakNet::UNASSIGNED_RAKNET_GUID)
		{
			//Update players
			if (startLerpingTimer <= 0.0f || !mIsServer)
			{
				mPlayer1Ship->Update(mDeltaTime.asSeconds());
				mPlayer2Ship->Update(mDeltaTime.asSeconds());
			}

			UpdateAsteroids(mDeltaTime.asSeconds());

			if (mIsServer)
			{
				mPlayer1Ship->updateBufferedInfo();
				mPlayer1Ship->UpdateInterpolation(mDeltaTime.asSeconds());
				mPlayer2Ship->updateBufferedInfo();
				mPlayer2Ship->UpdateInterpolation(mDeltaTime.asSeconds());

				HandleBulletToAsteroidCollision();
				HandlePlayerToAsteroidCollision();
			}
			else
			{
				if (mPlayer1Ship->GetOwnerGUID() == WowNetwork->GetPeer()->GetMyGUID())
				{
					mPlayer2Ship->updateBufferedInfo();
					if (startLerpingTimer <= 0.0f)
					{
						mPlayer2Ship->UpdateInterpolation(mDeltaTime.asSeconds());
					}
				}
				else if (mPlayer2Ship->GetOwnerGUID() == WowNetwork->GetPeer()->GetMyGUID())
				{
					mPlayer1Ship->updateBufferedInfo();
					if (startLerpingTimer <= 0.0f)
					{
						mPlayer1Ship->UpdateInterpolation(mDeltaTime.asSeconds());
					}
				}
			}
			HandleScore();


			if (startLerpingTimer > 0.0f)
			{
				startLerpingTimer -= mDeltaTime.asSeconds();
				if (startLerpingTimer <= 0.0f)
				{
					startLerpingTimer = -1.0f;
				}
			}
		}


		Draw();
	}
}

void GameManager::HandlePlayerToAsteroidCollision()
{//server only
	int numAsteroids = mAsteroids->size();
	for (int i = 0; i < numAsteroids; i++)
	{
		Asteroid* iterAsteroid = mAsteroids->at(i);

		//check collision with player 1
		if (!mPlayer1Ship->GetIsRespawning())
		{
			float xDistanceSquared = iterAsteroid->getPosition().x - mPlayer1Ship->getPosition().x;
			xDistanceSquared *= xDistanceSquared;
			float yDistanceSquared = iterAsteroid->getPosition().y - mPlayer1Ship->getPosition().y;
			yDistanceSquared *= yDistanceSquared;
			float distance = sqrt(xDistanceSquared + yDistanceSquared);
			//distance check between each asteroid and the player1 ship
			if (distance < ((iterAsteroid->GetRadius()) + (mPlayer1Ship->GetRadius() - 2.0f)))
			{
				SendPlayerAsteroidCollisionToClients(iterAsteroid->GetAsteroidID(), 1);
			}
		}
		if (!mPlayer2Ship->GetIsRespawning())
		{
			float xDistanceSquared = iterAsteroid->getPosition().x - mPlayer2Ship->getPosition().x;
			xDistanceSquared *= xDistanceSquared;
			float yDistanceSquared = iterAsteroid->getPosition().y - mPlayer2Ship->getPosition().y;
			yDistanceSquared *= yDistanceSquared;
			float distance = sqrt(xDistanceSquared + yDistanceSquared);
			//distance check between each asteroid and the player1 ship
			if (distance < ((iterAsteroid->GetRadius()) + (mPlayer2Ship->GetRadius() - 2.0f)))
			{
				SendPlayerAsteroidCollisionToClients(iterAsteroid->GetAsteroidID(), 2);
			}
		}
		
	}
}

void GameManager::SendPlayerAsteroidCollisionToClients(int asteroidID, int playerNum)
{//server
	RakNet::BitStream bsOut;
	bsOut.Write((RakNet::MessageID)ID_WOW_SYNC_BROADCAST_SHIP_ASTEROID_COLLISION);
	bsOut.Write(playerNum);
	bsOut.Write(asteroidID);
	WowNetwork->GetPeer()->Send(&bsOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, WowNetwork->GetServerGUID(), false);//send this only to the server
}

void GameManager::HandleSyncAsteroidShipCollision(int playerNum, int asteroidID)
{//clients and server call this
	assert(playerNum == 1 || playerNum == 2);
	//respawn player playerNum
	if (playerNum == 1)
	{
		mPlayer1Ship->Respawn();
	}
	else if (playerNum == 2)
	{
		mPlayer2Ship->Respawn();
	}

	mNumLives--;

	//destroy asteroid with asteroidID
	int numAsteroids = mAsteroids->size();
	for (int i = 0; i < numAsteroids; i++)
	{
		if (mAsteroids->at(i)->GetAsteroidID() == asteroidID)
		{
			Asteroid* killMe = mAsteroids->at(i);
			mAsteroids->erase(mAsteroids->begin() + i);
			delete killMe;
			killMe = NULL;
			return;
		}
	}
}

void GameManager::HandleScore()
{
	if (!mIsServer)
	{
		//if A is pressed, increment score by 100 every frame :)
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
		{
			if (hasAKeyBeenReleased)
			{//if A key is pressed and was not being pressed on the last frame...
				RakNet::BitStream bsOut;
				bsOut.Write((RakNet::MessageID)ID_WOW_CHEAT_REQUEST);
				bsOut.Write(CHEAT_TYPE_INCREMENT_SCORE);
				WowNetwork->GetPeer()->Send(&bsOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, WowNetwork->GetServerGUID(), false);//send this only to the server
			}
			hasAKeyBeenReleased = false;
		}
		else
		{
			hasAKeyBeenReleased = true;
		}
	}

	//update score
	mScoreText.setString("round: " + std::to_string(mRoundNum) + "\nscore: " + std::to_string(mScore));
	//center score text
	sf::FloatRect scoreTextRect = mScoreText.getLocalBounds();
	mScoreText.setOrigin(scoreTextRect.left + scoreTextRect.width / 2.0f, scoreTextRect.top + scoreTextRect.height / 2.0f);
	mScoreText.setPosition(Vector2((float)mWindow->getSize().x / 2.0f, (float)mScoreSize));

	//update lives
	mLivesText.setString("Lives: " + std::to_string(mNumLives));
	//center lives text
	sf::FloatRect livesTextRect = mLivesText.getLocalBounds();
	mLivesText.setOrigin(livesTextRect.left + livesTextRect.width / 2.0f, livesTextRect.top + livesTextRect.height / 2.0f);
	mLivesText.setPosition(Vector2((float)mWindow->getSize().x / 2.0f, (float)mWindow->getSize().y - (float)mScoreSize));

	//update round num
	mRoundNum = (int)(1 + (float)mScore / 5000.0f);

	//change level vars
	mSpawnTime = 0.2f + (2.0f / mRoundNum);
	minAsteroidSpeed = 0.5f + (mRoundNum / 10.0f);
	maxAsteroidSpeed = 1.5f + (mRoundNum / 10.0f);
	if (0.5f - ((float)mRoundNum * 0.025f) < 0.2)
	{
		mPlayer1Ship->SetRateOfFire(0.2f);
		mPlayer2Ship->SetRateOfFire(0.2f);
	}
	else
	{
		mPlayer1Ship->SetRateOfFire(0.5f - ((float)mRoundNum * 0.025f));
		mPlayer2Ship->SetRateOfFire(0.5f - ((float)mRoundNum * 0.025f));
	}

	if (5.0f + (0.25f * mRoundNum) > 15.0f)
	{
		mPlayer1Ship->SetBulletSpeed(15.0f);
		mPlayer2Ship->SetBulletSpeed(15.0f);
	}
	else
	{
		mPlayer1Ship->SetBulletSpeed(5.0f + (0.25f * mRoundNum));
		mPlayer2Ship->SetBulletSpeed(5.0f + (0.25f * mRoundNum));
	}
	
	if (mNumLives <= 0)
	{
		EndGame();
	}

}

void GameManager::EndGame()
{
	//Save round and score to text:
	isGameOver = true;
	endText.setString("game over\n\n your score: " + std::to_string(mScore));//+"Press Space to play again! :(((((
	endText.setColor(mScoreColor);
	//center lives text
	sf::FloatRect endTextRect = endText.getLocalBounds();
	endText.setOrigin(endTextRect.left + endTextRect.width / 2.0f, endTextRect.top + endTextRect.height / 2.0f);
	endText.setPosition(Vector2((float)mWindow->getSize().x / 2.0f, (float)mScoreSize));


	//Reset players:

}

void GameManager::ResetGame()
{
	mRoundNum = 1;
	mScore = 0;
}

void GameManager::HandleBulletToAsteroidCollision()
{
	int numAsteroids = mAsteroids->size();
	int numPlayer1Bullets = mPlayer1NaughtyList->size();
	int numPlayer2Bullets = mPlayer2NaughtyList->size();

	bool anyCollisions = false;//set this to true if there was any collision

	//iterate through asteroids as the main loop
	int i;
	for (i = 0; i < numAsteroids; i++)
	{
		//check collision between this asteroid and p1's bullets
		for (int j = 0; j < numPlayer1Bullets; j++)
		{
			if (j >= numPlayer1Bullets || i >= numAsteroids)
			{
				break;
			}
			bool hit = mAsteroids->at(i)->CheckCollisionWithBullet(mPlayer1NaughtyList->at(j));
			if (hit)
			{
				anyCollisions = true;
				Vector2 bulletVel = mPlayer1NaughtyList->at(j)->GetVelocity();
				DestroyAsteroid(i, bulletVel);
				DestroyBullet(1, j);//player 1 at index j

				numAsteroids--;
				numPlayer1Bullets--;
			}
		}
	}
	numAsteroids = mAsteroids->size();
	for (i = 0; i < numAsteroids; i++)
	{
		//check collision between this asteroid and p2's bullets
		for (int k = 0; k < numPlayer2Bullets; k++)
		{
			if (k >= numPlayer2Bullets || i >= numAsteroids)
			{
				break;
			}
			bool hit = mAsteroids->at(i)->CheckCollisionWithBullet(mPlayer2NaughtyList->at(k));
			if (hit)
			{
				anyCollisions = true;
				Vector2 bulletVel = mPlayer2NaughtyList->at(k)->GetVelocity();
				DestroyAsteroid(i, bulletVel);
				DestroyBullet(2, k);//player 2 at index k

				numAsteroids--;
				numPlayer2Bullets--;
			}
		}
	}
}

void GameManager::DestroyBullet(int whichPlayer, int index)
{
	assert(whichPlayer == 1 || whichPlayer == 2);//there's only two players
	if (mIsServer)
	{
		if (whichPlayer == 1)
		{
			Bullet* killMe = mPlayer1NaughtyList->at(index);

			SyncDestroyBullet(killMe);

			mPlayer1NaughtyList->erase(mPlayer1NaughtyList->begin() + index);
			delete killMe;
			killMe = NULL;
		}
		if (whichPlayer == 2)
		{
			Bullet* killMe = mPlayer2NaughtyList->at(index);

			SyncDestroyBullet(killMe);

			mPlayer2NaughtyList->erase(mPlayer2NaughtyList->begin() + index);
			delete killMe;
			killMe = NULL;
		}
	}
}

void GameManager::SyncDestroyBullet(Bullet* b)
{//tell all clients about this bullet that needs to be destroyed
	RakNet::BitStream bsOut;
	bsOut.Write((RakNet::MessageID)ID_WOW_SYNC_CLIENT_DESTROY_BULLET);
	bsOut.Write(b->GetBulletID());
	WowNetwork->GetPeer()->Send(&bsOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);//broadcast to all clients
}

void GameManager::DestroyBullet(int bulletID)
{//client -- destroy bullet because the server told me to
	int numP1Bullets = mPlayer1NaughtyList->size();
	int numP2Bullets = mPlayer2NaughtyList->size();
	for (int i = 0; i < numP1Bullets; i++)
	{
		if (mPlayer1NaughtyList->at(i)->GetBulletID() == bulletID)
		{
			Bullet* killMe = mPlayer1NaughtyList->at(i);

			mPlayer1NaughtyList->erase(mPlayer1NaughtyList->begin() + i);
			delete killMe;
			killMe = NULL;
			return;
		}
	}
	for (int i = 0; i < numP2Bullets; i++)
	{
		if (mPlayer2NaughtyList->at(i)->GetBulletID() == bulletID)
		{
			Bullet* killMe = mPlayer2NaughtyList->at(i);

			mPlayer2NaughtyList->erase(mPlayer2NaughtyList->begin() + i);
			delete killMe;
			killMe = NULL;
			return;
		}
	}
}

void GameManager::DestroyAsteroid(int index, Vector2 impactBulletVelocity)
{//server asteroid destroying
	Asteroid* killMe = mAsteroids->at(index);
	AsteroidSize size = killMe->GetAsteroidSize();
	mScore += (int)size * 100;//increment score
	Vector2 pos = killMe->getPosition();
	Vector2 vel = impactBulletVelocity;//killMe->GetVelocity();
	Vector2 killMeVel = killMe->GetVelocity();
	int id = killMe->GetAsteroidID();

	//destroy the asteroid after saving off its data
	mAsteroids->erase(mAsteroids->begin() + index);
	delete killMe;
	killMe = NULL;

	Vector2 vel1 = RotateVector(GetRandomFloatBetween(20.0f, 60.0f) * ((float)M_PI / 180.0f), vel);
	Vector2 vel2 = RotateVector(GetRandomFloatBetween(-60.0f, -20.0f) * ((float)M_PI / 180.0f), vel);

	vel1 /= 3.0f;
	vel2 /= 3.0f;

	//simply destroy the asteroid if it's a small one
	if (size <= SMALL_ASTEROID)
	{
		SyncSmallAsteroidDestroy(id);
		return;
	}

	//spawn asteroid 1
	Asteroid* newAsteroid1 = new Asteroid(pos, (AsteroidSize)((int)size - 1), vel1, sf::Color::White, mWindow);
	newAsteroid1->SetAsteroidID(mAsteroidIDCounter);
	mAsteroidIDCounter++;
	mAsteroids->push_back(newAsteroid1);

	//spawn asteroid 2
	Asteroid* newAsteroid2 = new Asteroid(pos, (AsteroidSize)((int)size - 1), vel2, sf::Color::White, mWindow);
	newAsteroid2->SetAsteroidID(mAsteroidIDCounter);
	mAsteroidIDCounter++;
	mAsteroids->push_back(newAsteroid2);

	SyncAsteroidDestroy(id, newAsteroid1->GetAsteroidID(), newAsteroid2->GetAsteroidID(), vel1, vel2);
}

void GameManager::SyncSmallAsteroidDestroy(int id)
{
	RakNet::BitStream bsOut;
	bsOut.Write((RakNet::MessageID)ID_WOW_SYNC_DESTROY_SMALL_ASTEROID);
	bsOut.Write(id);
	WowNetwork->GetPeer()->Send(&bsOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);//broadcast to all clients
}

void GameManager::SyncAsteroidDestroy(int destroyID, int id1, int id2, Vector2 vel1, Vector2 vel2)
{
	RakNet::BitStream bsOut;
	bsOut.Write((RakNet::MessageID)ID_WOW_SYNC_DESTROY_ASTEROID);
	bsOut.Write(destroyID);
	bsOut.Write(id1);
	bsOut.Write(vel1.x);
	bsOut.Write(vel1.y);
	bsOut.Write(id2);
	bsOut.Write(vel2.x);
	bsOut.Write(vel2.y);
	WowNetwork->GetPeer()->Send(&bsOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);//broadcast to all clients
}

void GameManager::ClientSyncSmallAsteroidDestroy(int id)
{
	mScore += (int)SMALL_ASTEROID * 100;//increment score
	int numAsteroids = mAsteroids->size();
	for (int i = 0; i < numAsteroids; i++)
	{
		if (mAsteroids->at(i)->GetAsteroidID() == id)
		{
			Asteroid* killMe = mAsteroids->at(i);
			mAsteroids->erase(mAsteroids->begin() + i);
			delete killMe;
			killMe = NULL;
			return;
		}
	}
}

void GameManager::ClientSyncAsteroidDestroy(int destroyID, int id1, int id2, Vector2 vel1, Vector2 vel2)
{
	int numAsteroids = mAsteroids->size();
	for (int i = 0; i < numAsteroids; i++)
	{
		if (mAsteroids->at(i)->GetAsteroidID() == destroyID)
		{
			Asteroid* killMe = mAsteroids->at(i);
			Vector2 pos = killMe->getPosition();
			AsteroidSize size = killMe->GetAsteroidSize();
			mScore += (int)size * 100;//increment score

			mAsteroids->erase(mAsteroids->begin() + i);
			delete killMe;
			killMe = NULL;

			//spawn asteroid 1
			Asteroid* newAsteroid1 = new Asteroid(pos, (AsteroidSize)((int)size - 1), vel1, sf::Color::White, mWindow);
			newAsteroid1->SetAsteroidID(id1);
			mAsteroids->push_back(newAsteroid1);

			//spawn asteroid 2
			Asteroid* newAsteroid2 = new Asteroid(pos, (AsteroidSize)((int)size - 1), vel2, sf::Color::White, mWindow);
			newAsteroid2->SetAsteroidID(id2);
			mAsteroids->push_back(newAsteroid2);

			return;
		}
	}
}

Vector2 GameManager::RotateVector(float radians, Vector2 vec)
{
	Vector2 newVec;
	newVec.x = (vec.x * cos(radians)) - (vec.y * sin(radians));
	newVec.y = (vec.x * sin(radians)) + (vec.y * cos(radians));
	return newVec;
}

void GameManager::UpdateAsteroids(float deltaTime)
{
	int numAsteroids = mAsteroids->size();
	for (int i = 0; i < numAsteroids; i++)
	{
		mAsteroids->at(i)->Update(deltaTime);
	}
	
	if (mIsServer)
	{
		mAsteroidSpawnTimer += deltaTime;
		if (mAsteroidSpawnTimer > mSpawnTime)
		{
			mAsteroidSpawnTimer = 0.0f;
			SpawnAsteroid();
		}
	}
}

void GameManager::SpawnAsteroid()
{//this should only be called by the server : the server tells the client to spawn them with these values
	Vector2 newPos = GetRandomPositionInDonutAroundCenterPoint(mWindow->getSize().x / 2.0f, mWindow->getSize().x / 1.5f);
	Vector2 lookAtPos = GetRandomPositionInDonutAroundCenterPoint(0, 150);

	float newSpeed = GetRandomFloatBetween(minAsteroidSpeed, maxAsteroidSpeed);

	//float newRotation = atan2(newPos.y, newPos.x) - atan2(lookAtPos.y, lookAtPos.x);
	float newRotation = atan2(lookAtPos.y - newPos.y, lookAtPos.x - newPos.x);
	if (newRotation < 0)
	{
		newRotation += 2.0f * (float)M_PI;
	}

	Vector2 newVelocity;
	newVelocity.x = cos(newRotation) * newSpeed;
	newVelocity.y = sin(newRotation) * newSpeed;

	int newSize = GetRandomIntBetween((int)(AsteroidSize::SMALL_ASTEROID), (int)(AsteroidSize::GIANT_ASTEROID));

	Asteroid* newAsteroid = new Asteroid(newPos, (AsteroidSize)newSize, newVelocity, sf::Color::White, mWindow);
	newAsteroid->SetAsteroidID(mAsteroidIDCounter);
	mAsteroidIDCounter++;
	mAsteroids->push_back(newAsteroid);

	BroadcastNewAsteroid(newAsteroid);
}

void GameManager::BroadcastNewAsteroid(Asteroid* newAsteroid)
{
	RakNet::BitStream bsOut;
	bsOut.Write((RakNet::MessageID)ID_WOW_SYNC_CLIENT_CREATE_ASTEROID);
	//sync ID
	int id = newAsteroid->GetAsteroidID();
	bsOut.Write(id);
	//sync pos
	Vector2 pos = newAsteroid->getPosition();
	bsOut.Write(pos.x);
	bsOut.Write(pos.y);
	//sync size
	AsteroidSize size = newAsteroid->GetAsteroidSize();
	bsOut.Write(size);
	//sync velocity
	Vector2 vel = newAsteroid->GetVelocity();
	bsOut.Write(vel.x);
	bsOut.Write(vel.y);
	WowNetwork->GetPeer()->Send(&bsOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);//broadcast to all clients
}

void GameManager::CreateAsteroid(Vector2 pos, AsteroidSize size, Vector2 vel, int id)
{
	Asteroid* newAsteroid = new Asteroid(pos, size, vel, sf::Color::White, mWindow);
	newAsteroid->SetAsteroidID(id);
	mAsteroids->push_back(newAsteroid);
}

Vector2 GameManager::GetRandomPositionInDonutAroundCenterPoint(float radiusMin, float radiusMax)
{
	//get random direction to apply our distance in
	float randRadians = GetRandomFloatBetween(0.0f, (2.0f * (float)M_PI));

	//get random distance between our min and max radii
	float randRadius = GetRandomFloatBetween(radiusMin, radiusMax);

	//translate these values into xy coords
	float xCoord = (cos(randRadians) * randRadius) + (mWindow->getSize().x / 2.0f);
	float yCoord = (sin(randRadians) * randRadius) + (mWindow->getSize().y / 2.0f);

	return Vector2(xCoord, yCoord);
}

void GameManager::Draw()
{
	redraw = false;
	//get ready to draw
	mWindow->clear(mClearToColor);

	//draw stuff:
	if (!isGameOver)
	{
		mWindow->draw(mScoreText);
		mWindow->draw(mLivesText);

		mPlayer1Ship->Draw();
		mPlayer2Ship->Draw();

		int numAsteroids = mAsteroids->size();
		for (int i = 0; i < numAsteroids; i++)
		{
			mAsteroids->at(i)->Draw();
		}
	}
	else
	{
		mWindow->draw(endText);
	}

	//end the draw
	mWindow->display();
}

RakNet::RakNetGUID GameManager::GetShipOwnerGUID(int whichPlayer)
{
	if (whichPlayer == 1)
	{
		return mPlayer1Ship->GetOwnerGUID();
	}
	else if (whichPlayer == 2)
	{
		return mPlayer2Ship->GetOwnerGUID();
	}
	return RakNet::UNASSIGNED_RAKNET_GUID;
}

void GameManager::SetShipOwnerGUID(int whichPlayer, RakNet::RakNetGUID newGUID)
{
	if (whichPlayer == 1)
	{
		mPlayer1Ship->SetOwnerGUID(newGUID, mGUID);
	}
	else if (whichPlayer == 2)
	{
		mPlayer2Ship->SetOwnerGUID(newGUID, mGUID);
	}
}

Ship* GameManager::GetShipFromGUID(RakNet::RakNetGUID owner)
{
	if (mPlayer1Ship->GetOwnerGUID() == owner)
	{
		return mPlayer1Ship;
	}
	else if (mPlayer2Ship->GetOwnerGUID() == owner)
	{
		return mPlayer2Ship;
	}
	printf("Error: GameManager->GetShipFromGUID: no ship found!");
	return nullptr;
}

sf::Vector2f GameManager::Lerp(sf::Vector2f start, sf::Vector2f end, float t)
{
	sf::Vector2f ls;
	ls.x = Lerp(start.x, end.x, t);
	ls.y = Lerp(start.y, end.y, t);
	return ls;
}

float GameManager::Lerp(float start, float end, float t)
{
	return (((1 - t) * start) + (t * end));
}

void GameManager::SyncShips(RakNet::BitStream& bs, bool sending)
{
	if (sending)
	{
		mPlayer1Ship->OnSyncSend(bs);
		mPlayer2Ship->OnSyncSend(bs);
	}
	else
	{
		mPlayer1Ship->OnSyncReceive(bs);
		mPlayer2Ship->OnSyncReceive(bs);
	}
}

void GameManager::SendLocalShip(RakNet::BitStream& bsOut)
{
	if (mPlayer1Ship->GetOwnerGUID() == mGUID)
	{//client 1 sends it's local player position to the server.
		bsOut.Write(1);
		mPlayer1Ship->OnSyncSend(bsOut);
	}
	else if (mPlayer2Ship->GetOwnerGUID() == mGUID)
	{//client 2 sends it's local player position to the server.
		bsOut.Write(2);
		mPlayer2Ship->OnSyncSend(bsOut);
	}
	else
	{
		printf("\nERROR: GameManager->SendLocalShip(): Client doesn't own ship");
	}
}

void GameManager::ReceiveShip(int playerNum, float packetTime, RakNet::BitStream& bsIn)
{
	PositionInfo playerInfo;
	playerInfo.timeStamp = (RakNet::Time)packetTime;

	if (playerNum == 1)
	{
		//mPlayer1Ship->SetTimeSinceLastPacket(packetTime);//mSyncTime = packetTime
		mPlayer1Ship->OnSyncReceive(bsIn, playerInfo);
		mPlayer1Ship->AddInfo(playerInfo);//mBufferedPlayerInfo.push_back(playerInfo);
	}
	else if (playerNum == 2)
	{
		//mPlayer2Ship->SetTimeSinceLastPacket(packetTime);//mSyncTime = packetTime
		mPlayer2Ship->OnSyncReceive(bsIn, playerInfo);
		mPlayer2Ship->AddInfo(playerInfo);//mBufferedPlayerInfo.push_back(playerInfo);
	}
}

int GameManager::CreateBullet(RakNet::RakNetGUID bulletOwner, Vector2 position, float rotation)
{//server calls this
	Ship* owner = GetShipFromGUID(bulletOwner);
	int newBulletID = -1;
	if (owner != nullptr)
	{
		owner->CreateBullet(position, rotation, newBulletID);
	}
	return newBulletID;
}

void GameManager::CreateBullet(RakNet::RakNetGUID bulletOwner, Vector2 position, float rotation, int newID)
{//clients call this after receiving instruction from server to create bullet
	Ship* owner = GetShipFromGUID(bulletOwner);
	int newBulletID = -1;
	if (owner != nullptr)
	{
		Bullet* newBullet = owner->CreateBullet(position, rotation, newBulletID);
		newBullet->SetBulletID(newID);
	}
}

float GameManager::GetRandomFloatBetween(float min, float max)
{//from a StackOverflow thread
	assert(min <= max);
	float random = ((float)rand()) / (float)RAND_MAX;

	float range = max - min;
	return (random*range) + min;
}

int GameManager::GetRandomIntBetween(int min, int max)
{
	assert(min <= max);
	return rand() % (max + 1 - min) + min;
}

void GameManager::incrementBulletIDs()
{
	mBulletIDCounter++;
	if (mBulletIDCounter == INT_MAX)
	{
		mBulletIDCounter = 0;
	}
}

void GameManager::IncrementAsteroidIDs()
{
	mAsteroidIDCounter++;
	if (mAsteroidIDCounter == INT_MAX)
	{
		mAsteroidIDCounter = 0;
	}
}

void GameManager::HandleCheat(CheatTypes cheatType)
{
	switch (cheatType)
	{
	case CHEAT_TYPE_INCREMENT_SCORE:
		mScore += 10000;//+10,000 to score
		break;
	default:
		break;
	}
}