#ifndef GAME_MANAGER_H
#define GAME_MANAGER_H

#include "SFML\Graphics.hpp"
#include "RakPeerInterface.h"
#include "Ship.h"
#include "Bullet.h"
#include "Asteroid.h"
#include <time.h>
#include <stdlib.h>
#include "WowPeer.h"
#include <string>



enum CheatTypes
{
	CHEAT_TYPE_INCREMENT_SCORE,//adds 10,000 points to score
};


class GameManager
{
public:
	GameManager();
	~GameManager();

	void Initialize(RakNet::RakNetGUID myGUID, bool isServer=false);
	void Cleanup();

	void Update();

	void Draw();

	//our SFML window
	int GetWindowWidth(){ return mWindow->getSize().x; }
	int GetWindowHeight(){ return mWindow->getSize().y; }

	inline void SetWindowTitle(std::string title){ mWindow->setTitle(title); }

	float GetFPS(){ return FPS; }

	RakNet::RakNetGUID GetShipOwnerGUID(int whichPlayer);
	void SetShipOwnerGUID(int whichPlayer, RakNet::RakNetGUID newGUID);

	Ship* GetShipFromGUID(RakNet::RakNetGUID owner);

	void SyncShips(RakNet::BitStream& bs, bool sending); //Called by server -> syncs all ships
	void SendLocalShip(RakNet::BitStream& bsOut); //Locally called by client -> sends my ship to server
	void ReceiveShip(int playerNum, float packetTime, RakNet::BitStream& bsIn);

	//returns new bullet ID
	int CreateBullet(RakNet::RakNetGUID bulletOwner, Vector2 position, float rotation);
	void CreateBullet(RakNet::RakNetGUID bulletOwner, Vector2 position, float rotation, int newID);
	void DestroyBullet(int bulletID);

	//Lerp
	sf::Vector2f Lerp(sf::Vector2f start, sf::Vector2f end, float t);
	float Lerp(float start, float end, float t);
	inline float GetDiscardTime(){ return mDiscardTime; }

	//Time dialation
	const float mDiscardTime = 0.1f;  //packet buffer time -> only buffer within this time

	//IDs
	void incrementBulletIDs();
	inline int getBulletIDCounter(){ return mBulletIDCounter; }

	void IncrementAsteroidIDs();
	inline int GetAsteroidIDCounter(){ return mAsteroidIDCounter; }

	//server tells clients to create asteroids, clients create them with this funk
	void CreateAsteroid(Vector2 pos, AsteroidSize size, Vector2 vel, int id);
	//clients receive this data
	void ClientSyncSmallAsteroidDestroy(int id);
	void ClientSyncAsteroidDestroy(int destroyID, int id1, int id2, Vector2 vel1, Vector2 vel2);

	void HandleCheat(CheatTypes cheatType);

	void HandleSyncAsteroidShipCollision(int playerNum, int asteroidID);

private:
	//update the local game's draw and update at 60 FPS
	const float FPS = 1.0f / 60.0f;
	sf::Time mDeltaTime;

	//SFML variables
	sf::Clock mClock;
	sf::RenderWindow* mWindow;
	sf::Event ev;
	bool redraw;
	const sf::Color mClearToColor = sf::Color::Black;

	//list of all objects
	std::vector<WowObject*>* mObjects;

	//player ships
	Ship* mPlayer1Ship;
	Ship* mPlayer2Ship;

	//this peer's guid
	RakNet::RakNetGUID mGUID;

	bool mIsServer;

	//ROUNDS
	int mRoundNum;

	//handle asteroids
	std::vector<Asteroid*>* mAsteroids;
	void UpdateAsteroids(float deltaTime);
	void SpawnAsteroid();
	float minAsteroidSpeed;
	float maxAsteroidSpeed;
	float mAsteroidSpawnTimer;
	float mSpawnTime;
	void DestroyAsteroid(int index, Vector2 impactBulletVelocity);
	//server broadcasts destroyed asteroids
	void SyncSmallAsteroidDestroy(int id);
	void SyncAsteroidDestroy(int destroyID, int id1, int id2, Vector2 vel1, Vector2 vel2);
	


	//utils
	float GetRandomFloatBetween(float min, float max);
	int GetRandomIntBetween(int min, int max);
	Vector2 GetRandomPositionInDonutAroundCenterPoint(float radiusMin, float radiusMax);
	Vector2 RotateVector(float radians, Vector2 vec);
	
	//store all the bullets we fire -- we will get a reference to this once -- the actual list is stored in our ships
	std::vector<Bullet*>* mPlayer1NaughtyList;//container for our bullets P1
	std::vector<Bullet*>* mPlayer2NaughtyList;//container for our bullets P2

	
	void HandleBulletToAsteroidCollision();
	void HandlePlayerToAsteroidCollision();
	void SendPlayerAsteroidCollisionToClients(int asteroidID, int playerNum);
	void DestroyBullet(int whichPlayer, int index);
	int mBulletIDCounter;
	int mAsteroidIDCounter;

	//scoring
	int mScore;//std::to_string(mScore)
	sf::Font mFont;
	sf::Text mScoreText;
	const sf::Color mScoreColor = sf::Color(255, 215, 0);
	const int mScoreSize = 50;
	void HandleScore();
	bool hasAKeyBeenReleased;
	int mNumLives;
	const int TOTAL_NUM_LIVES = 4;
	sf::Text mLivesText;

private:
	void SyncDestroyBullet(Bullet* newBullet);
	void BroadcastNewAsteroid(Asteroid* newAsteroid);

	//let's fix the lerp issues!
	float startLerpingTimer;
	
	//the homestretch
	void EndGame();
	void ResetGame();
	sf::Text endText;
	bool isGameOver;
};

extern GameManager* GM;

#endif