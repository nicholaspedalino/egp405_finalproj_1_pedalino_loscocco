######Masteroids! - by Nick Pedalino and Mike Loscocco######
############################################################

https://bitbucket.org/nicholaspedalino/ego405_nick_pedalino_mike_loscocco_asteroids

#Controls
+ Mouse to look
+ Left click to shoot
+ W key to move forward in the direction you're looking at.
+ A key to cheat and add 10,000 points

#Features
+ Vintage Christmas aesthetic
+ Time Dilation
+ Rounds that increase player shot speed, rate of fire, rate of asteroid spawning, asteroid speed
+ Next gen linear velocity-based physics
+ It's networked idk there's also lots of other stuff
+ Scoring system that gives you a false sense of accomplishment with lots of 0's

#etc---
run.bat might execute one server and two clients, but it also might not. It's pretty unpredictable.
If you want to try though, run.bat MUST be put into your build's Debug folder.

#Time Dilation Code:
-struct PositionInfo defined in Ship.h
-There's a 100 ms delay in GameManager -> mDiscardTime
-ID_WOW_SYNC_RECEIVE_PLAYERS -> gets a delta time from the timestamp in the packet
-GameManager::ReceiveShip() -> constructs a PositionInfo struct with the time difference, passes that struct to Ship::ReceiveShip(), which then edits the struct with the player position info read from the bitstream
Lastly, it adds the PositionInfo to the ship's buffer
-Ship::updateBufferedInfo() -> prunes the buffer based on how old the information is
sets initial and final position based off the info in the buffer
-Ship::UpdateInterpolation() -> lerps the position based on the initial and final position from the packets