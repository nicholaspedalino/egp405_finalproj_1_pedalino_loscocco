#include "Ship.h"
#include "BitStream.h"
#include "GameManager.h"

#include "WowClient.h"
#include "GetTime.h"

# define M_PI           3.14159265358979323846  /* pi */

Ship::Ship(Vector2 pos, sf::Color outlineColor, sf::Color fillColor, float speed, sf::RenderWindow* window)
{
	mWindow = window;
	mPosition = pos;
	mSpeed = speed;
	mOutlineColor = outlineColor;
	mFillColor = fillColor;
	mRotation = 0.0f;

	if (mOutlineColor == sf::Color::Green)
	{
		mBulletColor = sf::Color::Red;
	}
	else if (mOutlineColor == sf::Color::Red)
	{
		mBulletColor = sf::Color::Green;
	}
	else
	{
		mBulletColor = sf::Color::White;
	}

	mTriangle = sf::CircleShape(10, 3);//circle with 3 vertices = trinagle
	mTriangle.setFillColor(mFillColor);
	mTriangle.setOutlineColor(mOutlineColor);
	mTriangle.setPosition(mPosition);
	mTriangle.setOutlineThickness(2);
	mTriangle.setOrigin(mTriangle.getRadius(), mTriangle.getRadius());

	mOwnerGUID = RakNet::UNASSIGNED_RAKNET_GUID;
	mObjectID = ID_SHIP;

	mMousePos = Vector2(0, 0);
	mIsControlledLocally = false;

	mNaughtyList = new std::vector<Bullet*>();
	mShotTimer = ROF;
	mCanShoot = true;

	mPositionFinal = mPosition;
	mVelocityFinal = mVelocity;
	mRotationFinal = mRotation;
	mPositionInitial = mPosition;
	mVelocityInitial = mVelocity;
	mRotationInitial = mRotation;

	mLerpT = 0.0f;

	respawnTimer = RESPAWN_TIME;
	isRespawning = false;
}

Ship::~Ship()
{
}

void Ship::Update(float deltaTime)
{
	//Update input, and other stuff that should only be accessed locally
	//if (mOwnerGUID != RakNet::UNASSIGNED_RAKNET_GUID && mIsControlledLocally)
	if (mOwnerGUID != RakNet::UNASSIGNED_RAKNET_GUID && mOwnerGUID == WowNetwork->GetPeer()->GetMyGUID())
	{
		GetInput();

		HandleRotation();
		HandleMove(deltaTime);

		if (!isRespawning)
		{//if respawning, we can move but not shoot.
			HandleShooting(deltaTime);
		}
	}

	//handle respawning
	if (isRespawning)
	{
		respawnTimer -= deltaTime;
		if (respawnTimer < 0.0f)
		{//end respawn
			isRespawning = false;
			mTriangle.setOutlineColor(mOutlineColor);
		}
	}

	//update bullets
	unsigned int numBullets = mNaughtyList->size();
	for (unsigned int i = 0; i < numBullets; i++)
	{
		mNaughtyList->at(i)->Update(deltaTime);
	}

	HandleBulletDestruction();

	//Update sprite positioning
	mTriangle.setPosition(mPosition);
	mTriangle.setRotation(mRotation + 90);

	//HandleBounds:
	if (mPosition.x + mTriangle.getScale().x > mWindow->getSize().x) //X+
	{
		mPosition.x = 0-mTriangle.getScale().x;
	}
	if (mPosition.x < -mTriangle.getScale().x) //X-
	{
		mPosition.x = mWindow->getSize().x - mTriangle.getScale().x;
	}
	if (mPosition.y + mTriangle.getScale().y > mWindow->getSize().y)//Y+
	{
		mPosition.y = 0 + mTriangle.getScale().y;
	}
	if (mPosition.y < -mTriangle.getScale().y) //Y-
	{
		mPosition.y = mWindow->getSize().y - mTriangle.getScale().y;
	}


	//DELETE THIS WHEN THE INTERPOLATION IS DONE /////////////////////////////////////////////////////////////////////////////////////////////
	/*mPosition = mPositionFinal;
	mRotation = mRotationFinal;
	mVelocity = mVelocityFinal;*/
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}

void Ship::UpdateInterpolation(float deltaTime)
{
	//std::cout << "\n LERPT " << mLerpT << "  POS F X " << mPositionFinal.x << " POS F Y " << mPositionFinal.y << std::endl
		//<< " pos i x" << mPositionInitial.x << " pos i y " << mPositionInitial.y;

	mPosition = GM->Lerp(mPositionInitial, mPositionFinal, mLerpT);
	mRotation = GM->Lerp(mRotationInitial, mRotationFinal, mLerpT);
	mVelocity = GM->Lerp(mVelocityInitial, mVelocityFinal, mLerpT);

	if (mLerpT < 1.0f)
	{
		mLerpT += deltaTime / GM->GetDiscardTime();
	}

	mTriangle.setPosition(mPosition);
	mTriangle.setRotation(mRotation);

	//std::cout << "LERP T " << mLerpT << "  TIME BETWEEN PACKETS: " << mSyncTime << std::endl;
}

void Ship::HandleBulletDestruction()
{
	unsigned int numBullets = mNaughtyList->size();
	int markedForDeletion = -1;
	for (unsigned int i = 0; i < numBullets; i++)
	{
		if (mNaughtyList->at(i) != nullptr && mNaughtyList->at(i)->GetAge() > BULLET_LIFESPAN)
		{
			//NaughtyList->erase(mNaughtyList->begin() + i);
			markedForDeletion = i;
			break;
		}
	}
	if (markedForDeletion != -1)
	{
		Bullet* deleteMe = mNaughtyList->at(markedForDeletion);//save this off so we can properly delete it
		mNaughtyList->erase(mNaughtyList->begin() + markedForDeletion);//remove this element from the vector
		delete deleteMe;
		deleteMe = NULL;
	}
}

void Ship::HandleShooting(float deltaTime)
{
	if (mCanShoot && mIsMouseClickDown)
	{
		mShotTimer = ROF;
		mCanShoot = false;
		ShootBullet();
	}

	if (!mCanShoot)
	{
		mShotTimer -= deltaTime;
		if (mShotTimer <= 0.0f)
		{
			mCanShoot = true;
		}
	}
}

void Ship::ShootBullet()
{//server does not get here. So we shouldn't actually spawn a bullet here, but request the server allow us to spawn a bullet.
	//Bullet* newBullet = CreateBullet(mPosition, mRotation);// new Bullet(mPosition, mRotation, BULLET_SPEED, mBulletColor, mWindow);
	//mNaughtyList->push_back(newBullet);

	//Tell the server to spawn this bullet at this position with this rotation:
	RakNet::BitStream bsOut;
	bsOut.Write((RakNet::MessageID)ID_WOW_SYNC_CREATE_BULLET);
	bsOut.Write(mPosition.x);
	bsOut.Write(mPosition.y);
	bsOut.Write(mRotation);
	static_cast<WowClient*>(WowNetwork)->SendToServer(bsOut, false);
}

Bullet* Ship::CreateBullet(Vector2 position, float rotation, int& newID)
{
	Bullet* newBullet = new Bullet(position, rotation, BULLET_SPEED, mBulletColor, mWindow);
	newBullet->SetBulletID(GM->getBulletIDCounter());
	GM->incrementBulletIDs();
	mNaughtyList->push_back(newBullet);

	newID = newBullet->GetBulletID();

	return newBullet;
}

Bullet* Ship::CreateBullet(Vector2 position, float rotation)
{
	Bullet* newBullet = new Bullet(position, rotation, BULLET_SPEED, mBulletColor, mWindow);
	mNaughtyList->push_back(newBullet);

	return newBullet;
}

void Ship::HandleMove(float deltaTime)
{
	if (mIsWKeyDown)
	{
		mVelocity.x = cos(mRotation * (float)(M_PI) / 180.0f) * mSpeed;
		mVelocity.y = sin(mRotation * (float)(M_PI) / 180.0f) * mSpeed;
		
		//check distance between ship and mouse
		float xDistance = (mPosition.x + mVelocity.x) - mMousePos.x;
		float yDistance = (mPosition.y + mVelocity.y) - mMousePos.y;
		float distanceToMouse = sqrt((xDistance * xDistance) + (yDistance * yDistance));

		if (distanceToMouse > mSpeed + 0.5f)
		{//if not that close to the mouse, move normally
			mPosition += mVelocity;
		}
		else
		{//if pretty close to the mouse, snap to the mouse
			mPosition = mMousePos;
		}
	}
	else
	{
		mVelocity = Vector2(0, 0);
	}
}

void Ship::HandleRotation()
{
	//check distance from ship to mouse
	float xDistance = mPosition.x - mMousePos.x;
	float yDistance = mPosition.y - mMousePos.y;
	float distanceToMouse = sqrt((xDistance * xDistance) + (yDistance * yDistance));

	if (distanceToMouse > mSpeed + 2.0f)
	{//rotate normally if not close to the mouse
		float x = mMousePos.x - mPosition.x;
		float y = mMousePos.y - mPosition.y;

		mRotation = ((atan2(y, x)) * (180.0f / (float)M_PI));
	}
	else
	{//don't rotate
	}
	

	mTriangle.setRotation(mRotation + 90);
}

void Ship::GetInput()
{
	mIsWKeyDown = sf::Keyboard::isKeyPressed(sf::Keyboard::W);
	mIsMouseClickDown = sf::Mouse::isButtonPressed(sf::Mouse::Button::Left);

	mMousePos = (Vector2)sf::Mouse::getPosition(*mWindow);
}

void Ship::Draw()
{
	mWindow->draw(mTriangle);

	//draw bullets
	unsigned int numBullets = mNaughtyList->size();
	for (unsigned int i = 0; i < numBullets; i++)
	{
		mNaughtyList->at(i)->Draw();
	}
}

void Ship::SetOwnerGUID(RakNet::RakNetGUID newOwnerGUID, RakNet::RakNetGUID myGUID)
{
	mOwnerGUID = newOwnerGUID;
	if (myGUID == newOwnerGUID)
	{
		mIsControlledLocally = true;
	}
}

void Ship::OnSyncSend(RakNet::BitStream& bsOut)
{
	//Write the player's info to the bitstream
	bsOut.Write(mPosition.x);
	bsOut.Write(mPosition.y);
	bsOut.Write(mRotation);
	bsOut.Write(mVelocity.x);
	bsOut.Write(mVelocity.y);
	bsOut.Write(mOwnerGUID);
}
void Ship::OnSyncReceive(RakNet::BitStream& bsIn)
{//This is never called by client1, client2, or the server (I checked.) but we need it for the override I guess..
}

void Ship::OnSyncReceive(RakNet::BitStream& bsIn, PositionInfo& info)
{//&info is populated with data from the bitstream, sent back to the calling line, and then that info is sent to AddInfo();
	Vector2 pos;
	float rot;
	Vector2 vel;
	RakNet::RakNetGUID ownerGUID;

	bsIn.Read(pos.x);
	bsIn.Read(pos.y);
	bsIn.Read(rot);
	bsIn.Read(vel.x);
	bsIn.Read(vel.y);
	bsIn.Read(ownerGUID);

	info.position = pos;
	info.rotation = rot;
	info.velocity = vel;
	info.playerGUID = ownerGUID;
}

void Ship::AddInfo(PositionInfo infoToAdd)
{
	mBufferedPlayerInfo.push_back(infoToAdd);
}


void Ship::updateBufferedInfo()
{
	//Handle outdated buffered info
	RakNet::Time currentTime = RakNet::GetTime();
	for (unsigned int i = mBufferedPlayerInfo.size(); i-- > 0;)
	{
		PositionInfo temp = mBufferedPlayerInfo.at(i);
		if (temp.timeStamp < currentTime - GM->GetDiscardTime())
		{
			mPositionInitial = mPosition;
			mRotationInitial = mRotation;
			mVelocityInitial = mVelocity;

			mPositionFinal = mBufferedPlayerInfo[i].position;
			mRotationFinal = mBufferedPlayerInfo[i].rotation;
			mVelocityFinal = mBufferedPlayerInfo[i].velocity;

			mBufferedPlayerInfo.erase(mBufferedPlayerInfo.begin(), mBufferedPlayerInfo.begin() + i);
			break;
		}
	}
}

void Ship::Respawn()
{
	isRespawning = true;
	Vector2 newPos = Vector2(mWindow->getSize().x / 2.0f, mWindow->getSize().y / 2.0f);
	mPosition = newPos;
	mPositionInitial = newPos;
	mPositionFinal = newPos;
	mRotation = 0.0f;

	mTriangle.setPosition(newPos);
	mTriangle.setRotation(0.0f);
	mTriangle.setOutlineColor(RESPAWN_COLOR);

	respawnTimer = RESPAWN_TIME;
}