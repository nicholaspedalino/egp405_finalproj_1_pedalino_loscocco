#ifndef SHIP_H
#define SHIP_H

#include "SFML\Graphics.hpp"
#include "WowObject.h"
#include "Bullet.h"
#include <vector>

#pragma pack(push, 1)
struct PositionInfo
{
	PositionInfo(){}
	PositionInfo(Vector2 pos, float rot, Vector2 vel, RakNet::Time stamp)
	{
		position = pos;
		rotation = rot;
		velocity = vel;
		timeStamp = stamp;
	}
	Vector2 position;
	Vector2 velocity;
	float rotation;
	RakNet::Time timeStamp;
	RakNet::RakNetGUID playerGUID;
};
#pragma pack(pop)

class Ship : public WowObject
{
public:
	Ship(Vector2 pos, sf::Color outlineColor, sf::Color fillColor, float speed, sf::RenderWindow* window);
	~Ship();

	virtual void Update(float deltaTime);
	virtual void UpdateInterpolation(float deltaTime);
	virtual void Draw();

	inline RakNet::RakNetGUID GetOwnerGUID(){ return mOwnerGUID; }
	void SetOwnerGUID(RakNet::RakNetGUID newOwnerGUID, RakNet::RakNetGUID myGUID);

	virtual void OnSyncSend(RakNet::BitStream& bsOut) override;
	virtual void OnSyncReceive(RakNet::BitStream& bsIn) override;
	void OnSyncReceive(RakNet::BitStream& bsIn, PositionInfo& info);

	Bullet* CreateBullet(Vector2 position, float rotation, int& newID);
	Bullet* CreateBullet(Vector2 position, float rotation);

	//inline float GetTimeSinceLastPacket(){ return mSyncTime; }
	//inline void SetTimeSinceLastPacket(float time){ mSyncTime = time; }

	std::vector<Bullet*>* GetNaughtyList(){ return mNaughtyList; }

	//lerp
	void updateBufferedInfo();
	void AddInfo(PositionInfo infoToAdd);

	void SetBulletSpeed(float newSpeed){ BULLET_SPEED = newSpeed; }
	void SetRateOfFire(float newROF){ ROF = newROF; }

	float GetRadius(){ return mTriangle.getRadius(); }

	void Respawn();

	bool GetIsRespawning(){ return isRespawning; }

private:
	RakNet::RakNetGUID mOwnerGUID;
	bool mIsControlledLocally;

	sf::CircleShape mTriangle;
	sf::Color mOutlineColor;
	sf::Color mFillColor;

	//input
	bool mIsWKeyDown;//move forward
	bool mIsMouseClickDown;//shoot
	//aim with mouse position

	float mSpeed;
	Vector2 mVelocity;
	float mRotation;

	float mRotationInitial;
	Vector2 mPositionInitial;
	Vector2 mVelocityInitial;
	float mRotationFinal;
	Vector2 mPositionFinal;
	Vector2 mVelocityFinal;

	void GetInput();
	void HandleRotation();
	void HandleMove(float deltaTime);
	void HandleShooting(float deltaTime);
	void HandleBulletDestruction();
	const float BULLET_LIFESPAN = 3.0f;

	Vector2 mMousePos;

	sf::RenderWindow* mWindow;

	//store all the bullets we fire
	std::vector<Bullet*>* mNaughtyList;//container for our bullets
	float mShotTimer;
	float ROF = 0.5f;//bullets per second
	bool mCanShoot;//can I shoot a bullet?
	void ShootBullet();
	sf::Color mBulletColor;
	float BULLET_SPEED = 5.0f;

	//Interpolation info
	//float mSyncTime;
	float mLerpT;
	std::vector<PositionInfo> mBufferedPlayerInfo;

	const float RESPAWN_TIME = 3;//in seconds
	float respawnTimer;
	bool isRespawning;
	const sf::Color RESPAWN_COLOR = sf::Color(90, 90, 90);
	
};

#endif