#include "WowClient.h"
#include "GetTime.h"

WowClient::WowClient(int serverPort, const char* serverAddress, int clientPort)
{
	mServerPort = serverPort;
	mServerAddress = serverAddress;
	mClientPort = clientPort;
}

WowClient::~WowClient()
{
	WowPeer::cleanup();
}

void WowClient::initialize()
{
	WowPeer::initialize();
}

void WowClient::connect()
{
	//disallow connection responses from any IP
	mPeer->AllowConnectionResponseIPMigration(false);

	//SocketDescriptor describes the local socket to use for RakPeer::Startup
	RakNet::SocketDescriptor socketDescriptor(mClientPort, 0);
	socketDescriptor.socketFamily = AF_INET;
	mPeer->Startup(1, &socketDescriptor, 1);
	mPeer->SetOccasionalPing(true);//send a ping every now and again

	//Connect to the specified host (ip or domain name) and server port
	RakNet::ConnectionAttemptResult car = mPeer->Connect(mServerAddress, mServerPort, "Rumpelstiltskin", (int)strlen("Rumpelstiltskin"));
	RakAssert(car == RakNet::CONNECTION_ATTEMPT_STARTED);

	printf("\nMy IP addresses:\n");
	for (unsigned int i = 0; i < mPeer->GetNumberOfAddresses(); i++)
	{
		RakNet::SystemAddress sa = mPeer->GetInternalID(RakNet::UNASSIGNED_SYSTEM_ADDRESS, i);
		printf("%i. %s (LAN=%i)\n", i + 1, sa.ToString(false), sa.IsLANAddress());
	}
}

void WowClient::update()
{
	RakNet::Packet* p = nullptr;

	for (p = mPeer->Receive(); p; mPeer->DeallocatePacket(p), p = mPeer->Receive())
	{
		// We got a packet, get the identifier with our handy function
		mPacketID = GetPacketIdentifier(p);

		// Check if this is a network message packet
		switch (mPacketID)
		{
			//COMMON:
		case ID_CONNECTION_LOST:
			// Couldn't deliver a reliable packet - i.e. the other system was abnormally
			// terminated
			printf("ID_CONNECTION_LOST\n");
			break;

			//CLIENT:
		case ID_ALREADY_CONNECTED:
			// Connection lost normally
			printf("ID_ALREADY_CONNECTED with guid %" PRINTF_64_BIT_MODIFIER "u\n", p->guid);
			break;
		case ID_INCOMPATIBLE_PROTOCOL_VERSION:
			printf("ID_INCOMPATIBLE_PROTOCOL_VERSION\n");
			break;
		case ID_REMOTE_DISCONNECTION_NOTIFICATION: // Server telling the clients of another client disconnecting gracefully.  You can manually broadcast this in a peer to peer enviroment if you want.
			printf("ID_REMOTE_DISCONNECTION_NOTIFICATION\n");
			break;
		case ID_REMOTE_CONNECTION_LOST: // Server telling the clients of another client disconnecting forcefully.  You can manually broadcast this in a peer to peer enviroment if you want.
			printf("ID_REMOTE_CONNECTION_LOST\n");
			break;
		case ID_REMOTE_NEW_INCOMING_CONNECTION: // Server telling the clients of another client connecting.  You can manually broadcast this in a peer to peer enviroment if you want.
			printf("ID_REMOTE_NEW_INCOMING_CONNECTION\n");
			break;
		case ID_CONNECTION_BANNED: // Banned from this server
			printf("We are banned from this server.\n");
			break;
		case ID_CONNECTION_ATTEMPT_FAILED:
			printf("Connection attempt failed\n");
			break;
		case ID_NO_FREE_INCOMING_CONNECTIONS:
			// Sorry, the server is full.
			printf("ID_NO_FREE_INCOMING_CONNECTIONS\n");
			break;
		case ID_INVALID_PASSWORD:
			printf("ID_INVALID_PASSWORD\n");
			break;

		case ID_CONNECTION_REQUEST_ACCEPTED:
			// This tells the client they have connected
			printf("ID_CONNECTION_REQUEST_ACCEPTED to %s with GUID %s\n", p->systemAddress.ToString(true), p->guid.ToString());
			printf("My external address is %s\n", mPeer->GetExternalID(p->systemAddress).ToString(true));
			printf("My GUID is: %s\n", mPeer->GetMyGUID().ToString());
			mServerGUID = p->guid;
			break;

			//WOW CASES
		case (RakNet::MessageID)ID_WOW_SYNC_INIT:
		{
			RakNet::BitStream bsIn(p->data, p->length, false);
			bsIn.IgnoreBytes(sizeof(RakNet::MessageID));
			RakNet::RakNetGUID player1, player2;
			bsIn.Read(player1);
			bsIn.Read(player2);

			GM->SetShipOwnerGUID(1, player1);
			GM->SetShipOwnerGUID(2, player2);
			break;
		}
		case (RakNet::MessageID)ID_WOW_SYNC_RECEIVE_PLAYERS:
		{
			RakNet::BitStream bsIn(p->data, p->length, false);
			bsIn.IgnoreBytes(sizeof(RakNet::MessageID));

			RakNet::Time timeStamp;
			bsIn.Read(timeStamp);

			GM->ReceiveShip(1, static_cast<float>(timeStamp) / 1000.0f, bsIn);
			GM->ReceiveShip(2, static_cast<float>(timeStamp) / 1000.0f, bsIn);

			break;
		}
		case (RakNet::MessageID)ID_WOW_SYNC_CLIENT_CREATE_BULLET:
		{
			RakNet::BitStream bsIn(p->data, p->length, false);
			bsIn.IgnoreBytes(sizeof(RakNet::MessageID));

			//read bullet data
			RakNet::RakNetGUID ownerGUID;
			bsIn.Read(ownerGUID);

			int bulletID;
			bsIn.Read(bulletID);

			Vector2 pos;
			bsIn.Read(pos.x);
			bsIn.Read(pos.y);
			float rot;
			bsIn.Read(rot);

			//GM should be able to handle bullet creation from here.
			GM->CreateBullet(ownerGUID, pos, rot, bulletID);
			break;
		}
		case (RakNet::MessageID)ID_WOW_SYNC_CLIENT_DESTROY_BULLET:
		{
			RakNet::BitStream bsIn(p->data, p->length, false);
			bsIn.IgnoreBytes(sizeof(RakNet::MessageID));

			int bulletID;
			bsIn.Read(bulletID);
			GM->DestroyBullet(bulletID);
			break;
		}
		case (RakNet::MessageID)ID_WOW_SYNC_CLIENT_CREATE_ASTEROID:
		{
			RakNet::BitStream bsIn(p->data, p->length, false);
			bsIn.IgnoreBytes(sizeof(RakNet::MessageID));
			//ID
			int id;
			bsIn.Read(id);
			//pos
			Vector2 pos;
			bsIn.Read(pos.x);
			bsIn.Read(pos.y);
			//size
			AsteroidSize size;
			bsIn.Read(size);
			//velocity
			Vector2 vel;
			bsIn.Read(vel.x);
			bsIn.Read(vel.y);

			GM->CreateAsteroid(pos, size, vel, id);

			break;
		}
		case (RakNet::MessageID)ID_WOW_SYNC_DESTROY_ASTEROID:
		{
			RakNet::BitStream bsIn(p->data, p->length, false);
			bsIn.IgnoreBytes(sizeof(RakNet::MessageID));
			//id of the asteroid we want to delete
			int destroyID;
			bsIn.Read(destroyID);
			//New Asteroid 1
			int id1;
			bsIn.Read(id1);
			Vector2 vel1;
			bsIn.Read(vel1.x);
			bsIn.Read(vel1.y);
			//New Asteroid 2
			int id2;
			bsIn.Read(id2);
			Vector2 vel2;
			bsIn.Read(vel2.x);
			bsIn.Read(vel2.y);

			GM->ClientSyncAsteroidDestroy(destroyID, id1, id2, vel1, vel2);
			break;
		}
		case (RakNet::MessageID)ID_WOW_SYNC_DESTROY_SMALL_ASTEROID:
		{
			RakNet::BitStream bsIn(p->data, p->length, false);
			bsIn.IgnoreBytes(sizeof(RakNet::MessageID));
			//id of the asteroid we want to delete
			int destroyID;
			bsIn.Read(destroyID);

			GM->ClientSyncSmallAsteroidDestroy(destroyID);
			break;
		}
		case (RakNet::MessageID)ID_WOW_CHEAT_CONFIRM:
		{//clients should not get here, only the ser
			RakNet::BitStream bsIn(p->data, p->length, false);
			bsIn.IgnoreBytes(sizeof(RakNet::MessageID));
			CheatTypes cheatType;
			bsIn.Read(cheatType);
			GM->HandleCheat(cheatType);
			break;
		}
		case (RakNet::MessageID)ID_WOW_SYNC_RECEIVE_SHIP_ASTEROID_COLLISION:
			RakNet::BitStream bsIn(p->data, p->length, false);
			bsIn.IgnoreBytes(sizeof(RakNet::MessageID));
			int playerNum;
			int asteroidID;
			bsIn.Read(playerNum);
			bsIn.Read(asteroidID);
			GM->HandleSyncAsteroidShipCollision(playerNum, asteroidID);
			break;
		}
	}
}

void WowClient::UpdateRakNetSync()
{
	mDeltaTime += mRakNetTimer.getElapsedTime().asSeconds();
	if (mDeltaTime > mRakNetFPS)
	{
		RakNet::BitStream bsOut;
		bsOut.Write((RakNet::MessageID)ID_WOW_SYNC_SEND_PLAYER);
		RakNet::Time time = RakNet::GetTimeMS();
		bsOut.Write(time); //Timestamp the packet -- GetTimeMS is in MS, GetTime is in MS too.
		GM->SendLocalShip(bsOut); //Add ship data

		mPeer->Send(&bsOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, mServerGUID, false);
	}
}

void WowClient::SendToServer(RakNet::BitStream& bsOut, bool broadcast)
{
	mPeer->Send(&bsOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, mServerGUID, broadcast);
}