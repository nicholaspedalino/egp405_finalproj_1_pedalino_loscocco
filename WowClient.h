#ifndef WOWCLIENT_H
#define WOWCLIENT_H

#include "WowPeer.h"

class WowClient : public WowPeer
{
public:
	WowClient(int serverPort, const char* serverAddress, int clientPort);
	~WowClient();

	virtual void initialize();

	virtual void connect() override;
	virtual void update() override;
	virtual void UpdateRakNetSync() override;
	void SendToServer(RakNet::BitStream& bsOut, bool broadcast);
	//RakNet::RakNetGUID GetServerGUID(){ return mServerGUID; }

private:
	int mServerPort;
	const char* mServerAddress;
	int mClientPort;

	//RakNet::RakNetGUID mServerGUID;
};



#endif