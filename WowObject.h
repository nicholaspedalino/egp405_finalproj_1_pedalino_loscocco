#ifndef WOW_OBJECT
#define WOW_OBJECT

#include "SFML\Graphics.hpp"
#include "RakPeerInterface.h"

#pragma pack(push, 1)
enum ObjectID
{
	ID_NULL = -1,
	ID_SHIP,
	ID_ASTEROID,
	ID_BULLET
};
#pragma pack(pop)

typedef sf::Vector2f Vector2;

//all objects that inherit from WowObject will be given a NetworkID by default.
class WowObject
{
public:
	virtual void Update(float deltaTime) = 0;
	virtual void Draw() = 0;
	inline ObjectID GetObjectID(){ return mObjectID; }

	inline void setPosition(sf::Vector2f newPosition){ mPosition = newPosition; }
	inline sf::Vector2f getPosition(){ return mPosition; }

	virtual void OnSyncSend(RakNet::BitStream& bsOut) = 0;
	virtual void OnSyncReceive(RakNet::BitStream& bsIn) = 0;

protected:
	ObjectID mObjectID;

	Vector2 mPosition;
};


#endif