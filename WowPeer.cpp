#include "WowPeer.h"
#include <iostream>

WowPeer::WowPeer()
{
}

WowPeer::~WowPeer()
{
	cleanup();
}

void WowPeer::cleanup()
{
	if (this->mPeer)
	{
		this->mPeer->Shutdown(300);
		// We're done with the network
		RakNet::RakPeerInterface::DestroyInstance(mPeer);
	}
}

void WowPeer::initialize()
{
	//Instantiate the peer
	this->mPeer = RakNet::RakPeerInterface::GetInstance();

	//Record the first client that connects to pass it to the ping function
	RakNet::SystemAddress clientID = RakNet::UNASSIGNED_SYSTEM_ADDRESS;

	mDeltaTime = 0.0f;
}

unsigned char WowPeer::GetPacketIdentifier(RakNet::Packet* p)
{
	if (p == 0)
		return 255;

	if ((unsigned char)p->data[0] == ID_TIMESTAMP)
	{
		RakAssert(p->length > sizeof(RakNet::MessageID) + sizeof(RakNet::Time));
		return (unsigned char)p->data[sizeof(RakNet::MessageID) + sizeof(RakNet::Time)];
	}
	else
		return (unsigned char)p->data[0];
}