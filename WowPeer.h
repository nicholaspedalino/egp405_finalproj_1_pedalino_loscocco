#ifndef WOWPEER_H
#define WOWPEER_H

#include "RakPeerInterface.h"
#include <MessageIdentifiers.h>
#include "BitStream.h"

#include <iostream>
#include "GameManager.h"

class WowPeer
{
public:
	WowPeer();
	~WowPeer();

	virtual void initialize();
	virtual void cleanup();

	virtual void connect() = 0;
	virtual void update() = 0;
	virtual void UpdateRakNetSync() =0;

	unsigned char GetPacketIdentifier(RakNet::Packet* p);

	inline RakNet::RakPeerInterface* GetPeer(){ return mPeer; }

	RakNet::RakNetGUID GetServerGUID(){ return mServerGUID; }

protected:
	RakNet::RakPeerInterface* mPeer;

	unsigned char mPacketID;

	const int mMaxConnections = 2;

	sf::Clock mRakNetTimer;
	float mDeltaTime;
	const float mRakNetFPS = 1.0f / 10.0f;

	RakNet::RakNetGUID mServerGUID;
};

#pragma pack(push, 1)
enum WowPacketID
{
	ID_WOW_SYNC_INIT = ID_USER_PACKET_ENUM + 1, //server sends this to client, client creates all objects from ObjectIDs sent by the server
	//For syncing serialized data
	ID_WOW_SYNC_SEND_PLAYER, //Sends my player data to server
	ID_WOW_SYNC_RECEIVE_PLAYERS, //Sends all player data to clients
	ID_WOW_SYNC_CREATE_BULLET,
	ID_WOW_SYNC_CLIENT_CREATE_BULLET,//server broadcasts this to clients when it receives a CREATE_BULLET packet -- tells clients to create this bullet.
	ID_WOW_SYNC_CLIENT_DESTROY_BULLET,//server broadcasts this to all clients when a bullet is destroyed -- use bulletID to tell which one to destroy
	ID_WOW_SYNC_CLIENT_CREATE_ASTEROID,//server broadcasts this to all clients when it makes a new asteroid -- clients then create this new asteroid locally
	ID_WOW_SYNC_DESTROY_SMALL_ASTEROID,//server broadcasts this to all clients when it destroys a small asteroid -- clients then simply destroy that asteroid and nothing else
	ID_WOW_SYNC_DESTROY_ASTEROID,//server broadcasts this to all clients when it destroys an asteroid (not small) -- clients break the asteroid
	ID_WOW_CHEAT_REQUEST,//client sends this to the server when it wants to cheat
	ID_WOW_CHEAT_CONFIRM,//server sends this to clients after a cheat request, clients apply cheat
	ID_WOW_SYNC_BROADCAST_SHIP_ASTEROID_COLLISION,//server broadcasts player->asteroid collision to all clients
	ID_WOW_SYNC_RECEIVE_SHIP_ASTEROID_COLLISION,//clients receive this from server, respawn corresponding ship
};
#pragma pack(pop)

extern WowPeer* WowNetwork;

#endif