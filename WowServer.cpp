#include "WowServer.h"
#include "GetTime.h"

WowServer::WowServer(int serverPort)
{
	mServerPort = serverPort;
	mNumClientsConnected = 0;
}

WowServer::~WowServer()
{

}

void WowServer::initialize()
{
	WowPeer::initialize();
}

void WowServer::cleanup()
{
	WowPeer::cleanup(); //Disconnect and cleanup
}

void WowServer::connect()
{
	//set some kind of password, must define the string and length
	mPeer->SetIncomingPassword("Rumpelstiltskin", (int)strlen("Rumpelstiltskin"));
	//# MS go by, timeout peer with error message UNASSIGNED SYSTEM ADDRESS
	mPeer->SetTimeoutTime(30000, RakNet::UNASSIGNED_SYSTEM_ADDRESS);

	//SocketDescriptor describes the local socket to use for RakPeer::Startup
	RakNet::SocketDescriptor sd;
	sd.port = mServerPort;
	sd.socketFamily = AF_INET; //test IPV4

	//try and start the server
	RakNet::StartupResult b = mPeer->Startup(mMaxConnections, &sd, 1);// == RakNet::RAKNET_STARTED;
	mPeer->SetMaximumIncomingConnections(mMaxConnections);

	//if the server failed to start
	if (b != RakNet::RAKNET_STARTED)
	{
		puts("Server failed to start. Terminating.");
		exit(1);
	}

	//Ping the remote systems every so often.
	mPeer->SetOccasionalPing(true);

	//giving up on sending an unreliable message after (param#) MS
	mPeer->SetUnreliableTimeout(10000);


	DataStructures::List< RakNet::RakNetSocket2* > sockets;
	mPeer->GetSockets(sockets);
	printf("\nMy IP addresses:\n");

	for (unsigned int i = 0; i < mPeer->GetNumberOfAddresses(); i++)
	{
		RakNet::SystemAddress sa = mPeer->GetInternalID(RakNet::UNASSIGNED_SYSTEM_ADDRESS, i);
		printf("%i. %s (LAN=%i)\n", i + 1, sa.ToString(false), sa.IsLANAddress());
	}

	mServerGUID = mPeer->GetMyGUID();
}

void WowServer::update()
{
	RakNet::Packet* p = nullptr;

	for (p = mPeer->Receive(); p; mPeer->DeallocatePacket(p), p = mPeer->Receive())
	{
		// We got a packet, get the identifier with our handy function
		mPacketID = GetPacketIdentifier(p);

		// Check if this is a network message packet
		switch (mPacketID)
		{
	//COMMON:
		case ID_CONNECTION_LOST:
			// Couldn't deliver a reliable packet - i.e. the other system was abnormally
			// terminated
			printf("ID_CONNECTION_LOST\n");

			mNumClientsConnected--;
			break;

	//SERVER
		case ID_DISCONNECTION_NOTIFICATION:
			// Connection lost normally
			printf("ID_DISCONNECTION_NOTIFICATION from %s\n", p->systemAddress.ToString(true));;
			break;

		case ID_NEW_INCOMING_CONNECTION:
			//Somebody connected.  We have their IP now
		{
			printf("Remote internal IDs:\n");
			RakNet::SystemAddress internalId;
			int newConnectionIndex = 0;
			for (int index = 0; index < MAXIMUM_NUMBER_OF_INTERNAL_IDS; index++)
			{
			   RakNet::SystemAddress internalId = mPeer->GetInternalID(p->systemAddress, index);
			   if (internalId != RakNet::UNASSIGNED_SYSTEM_ADDRESS)
			   {
				   printf("%i. %s\n", index + 1, internalId.ToString(true));
				   newConnectionIndex = index;
			   }
			}
			//Handle the new incomming connection
			mNumClientsConnected++;
			OnNewConnection(p->guid);
			
			break;
		}

	//Pings
		case ID_CONNECTED_PING:
		case ID_UNCONNECTED_PING:
			printf("Ping from %s\n", p->systemAddress.ToString(true));
			break;

	//WOW
		case (RakNet::MessageID)ID_WOW_SYNC_SEND_PLAYER:
		{
			RakNet::BitStream bsIn(p->data, p->length, false);
			bsIn.IgnoreBytes(sizeof(RakNet::MessageID));

			RakNet::Time timeStamp;
			bsIn.Read(timeStamp);

			//float deltaTime = ((RakNet::GetTimeMS() - timeStamp)/1000.0f); //--GetTimeMS is in MS, GetTime is in MS too.
			int playerNum = -1;
			bsIn.Read(playerNum);

			GM->ReceiveShip(playerNum, static_cast<float>(timeStamp)/1000.0f, bsIn);
			break;
		}
			
		case (RakNet::MessageID)ID_WOW_SYNC_CREATE_BULLET:
		{
			RakNet::BitStream bsIn(p->data, p->length, false);
			bsIn.IgnoreBytes(sizeof(RakNet::MessageID));
			Vector2 pos;
			bsIn.Read(pos.x);
			bsIn.Read(pos.y);
			float rot;
			bsIn.Read(rot);

			int bulletID = GM->CreateBullet(p->guid, pos, rot);

			BroadcastNewBulletToClients(p->guid, pos, rot, bulletID);

			break;
		}
		case (RakNet::MessageID)ID_WOW_CHEAT_REQUEST:
		{
			RakNet::BitStream bsIn(p->data, p->length, false);
			bsIn.IgnoreBytes(sizeof(RakNet::MessageID));
			CheatTypes cheatType;
			bsIn.Read(cheatType);
			GM->HandleCheat(cheatType);

			RakNet::BitStream bsOut;
			bsOut.Write((RakNet::MessageID)ID_WOW_CHEAT_CONFIRM);
			bsOut.Write(cheatType);
			WowNetwork->GetPeer()->Send(&bsOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, RakNet::UNASSIGNED_RAKNET_GUID, true);//broadcast cheat confirm to all clients
			break;
		}
		case (RakNet::MessageID)ID_WOW_SYNC_BROADCAST_SHIP_ASTEROID_COLLISION:
		{
			RakNet::BitStream bsIn(p->data, p->length, false);
			bsIn.IgnoreBytes(sizeof(RakNet::MessageID));
			int playerNum;
			int asteroidID;
			bsIn.Read(playerNum);
			bsIn.Read(asteroidID);
			GM->HandleSyncAsteroidShipCollision(playerNum, asteroidID);

			RakNet::BitStream bsOut;
			bsOut.Write((RakNet::MessageID)ID_WOW_SYNC_RECEIVE_SHIP_ASTEROID_COLLISION);
			bsOut.Write(playerNum);
			bsOut.Write(asteroidID);
			WowNetwork->GetPeer()->Send(&bsOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, RakNet::UNASSIGNED_RAKNET_GUID, true);//broadcast cheat confirm to all clients
			break;
		}
		}
	}
}

void WowServer::BroadcastNewBulletToClients(RakNet::RakNetGUID ownerGUID, Vector2 pos, float rotation, int bulletID)
{
	RakNet::BitStream bsOut;
	bsOut.Write((RakNet::MessageID)ID_WOW_SYNC_CLIENT_CREATE_BULLET);
	bsOut.Write(ownerGUID);
	bsOut.Write(bulletID);
	bsOut.Write(pos.x);
	bsOut.Write(pos.y);
	bsOut.Write(rotation);
	mPeer->Send(&bsOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);//broadcast this to all clients.
}

void WowServer::OnNewConnection(RakNet::RakNetGUID newGUID)
{
	//Set the owner of a ship, server-side
	GM->SetShipOwnerGUID(mNumClientsConnected, newGUID);

	//Send all connected GUIDs to all clients (whether or not those GUIDs are unassigned)
	RakNet::BitStream bsOut;
	bsOut.Write((RakNet::MessageID)ID_WOW_SYNC_INIT);
	bsOut.Write(GM->GetShipOwnerGUID(1));
	bsOut.Write(GM->GetShipOwnerGUID(2));
	
	mPeer->Send(&bsOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);
}

void WowServer::UpdateRakNetSync()
{
	mDeltaTime += mRakNetTimer.getElapsedTime().asSeconds();
	if (mDeltaTime > mRakNetFPS)
	{
		//restart the clock
		mRakNetTimer.restart();
		//carry over remainder time into deltaTime
		mDeltaTime -= mRakNetFPS;

		//SYNC NETWORKED OBJECTS
		//SYNC PLAYERS:
		RakNet::BitStream bsOut;
		bsOut.Write((RakNet::MessageID) ID_WOW_SYNC_RECEIVE_PLAYERS);
		RakNet::Time currentTime = RakNet::GetTime();
		bsOut.Write(currentTime);

		GM->SyncShips(bsOut, true);
		mPeer->Send(&bsOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);
	}
}