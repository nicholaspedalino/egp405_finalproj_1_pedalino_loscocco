#ifndef WOWSERVER_H
#define WOWSERVER_H

#include "WowPeer.h"

class WowServer : public WowPeer
{
public:
	WowServer();
	WowServer(int serverPort);
	~WowServer();

	virtual void initialize();
	virtual void cleanup();

	virtual void connect() override;
	virtual void update() override;

	void OnNewConnection(RakNet::RakNetGUID newGUID);
	void CreatePaddle(RakNet::RakNetGUID ownerGUID);

	virtual void UpdateRakNetSync() override;

private:
	int mServerPort;
	int mNumClientsConnected;

private:
	void BroadcastNewBulletToClients(RakNet::RakNetGUID ownerGUID, Vector2 pos, float rotation, int bulletID);

	
};



#endif