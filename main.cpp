#include <iostream>
#include <string>

#include "WowServer.h"
#include "WowClient.h"
#include "GameManager.h"

WowPeer* WowNetwork;

void main(int argc, char** argv)
{
	//<Handle argument parsing and client/server creation> {
	std::string clientIDString;
	int clientID = -1;
	std::string serverIDString;
	int serverID = 666;
	std::string serverIPString;
	const char* serverIP;
	bool isServer;

	//WowPeer* peer;

	//if no command line arguments were entered
	if (argc <= 1)//the first argument is always the filepath to the .exe
	{
		std::string input;
		bool isValidInput = false;

		while (!isValidInput)
		{
			std::cout << "Enter 'S' for Server or 'C' for Client: ";
			std::cin >> input;
			std::cout << std::endl;

			if (input[0] == 's' || input[0] == 'S')
			{
				isValidInput = true;
				isServer = true;

				//get server IP
				//std::cout << "Enter IP address: ";
				//std::cin >> serverIPString;
				//std::cout << std::endl;
				//serverIP = serverIPString.c_str();

			}
			else if (input[0] == 'c' || input[0] == 'C')
			{
				isValidInput = true;
				isServer = false;

				//get the client port ID
				std::cout << "Enter client port ID: ";
				std::cin >> clientIDString;
				std::cout << std::endl;
				clientID = std::stoi(clientIDString);

				//get IP
				std::cout << "Enter IP address: ";
				std::cin >> serverIPString;
				std::cout << std::endl;
				serverIP = serverIPString.c_str();
			}
			else
			{
				std::cout << "INVALID INPUT\n";
			}
		}
	}
	else//if we are using command line args:
	{
		//server has 1 args passed in
		if (argc == 2)
		{
			isServer = true;
			serverIDString = argv[1];
			serverID = std::stoi(serverIDString);
		}
		//server has 3 arg passed in
		else if (argc == 4)
		{
			isServer = false;
			clientIDString = argv[1];
			clientID = std::stoi(clientIDString);
			serverIPString = argv[2];
			serverIP = serverIPString.c_str();
			serverIDString = argv[3];
			serverID = std::stoi(serverIDString);
		}
		else
		{
			std::cerr << "INVALID COMMAND LINE ARGS" << std::endl;
		}
	}

	if (isServer)
	{
		WowNetwork = new WowServer(serverID);
	}
	else
	{
		WowNetwork = new WowClient(serverID, serverIP, clientID);
	}

	//initialize the peer
	WowNetwork->initialize();
	WowNetwork->connect();

	//initialize GM
	GM = new GameManager();
	GM->Initialize(WowNetwork->GetPeer()->GetMyGUID(), isServer);
	GM->SetWindowTitle("Smashteroids");

	//main update loop
	while (true)
	{
		WowNetwork->update();
		GM->Update();
		WowNetwork->UpdateRakNetSync();
	}

	delete WowNetwork;
	delete GM;

	system("PAUSE");
}